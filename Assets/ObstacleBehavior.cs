using UnityEngine;

public class ObstacleBehavior : MonoBehaviour
{
    public float speedFactor = 40f;

    // Start is called before the first frame update
    void Awake()
    {
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector3.right/speedFactor);
    }
}
