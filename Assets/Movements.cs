using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movements : MonoBehaviour
{

    public float jumpForce = 8f;

    private bool _gameOver;
    private GameMaster _gameMaster;
    private Rigidbody2D _rb;
    
    void Awake()
    {
        _rb = GetComponent<Rigidbody2D>();
        _gameMaster = GameObject.Find("GameMaster").GetComponent<GameMaster>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space) && !_gameOver)
        {
            _rb.rotation = -45;
            _rb.velocity = Vector2.up * jumpForce;
        }

        if (_rb.rotation <= 0)
        {
            _rb.rotation += 0.5f;
        }
    }

    private void OnCollisionEnter2D(Collision2D col)
    {
        _gameMaster.GameOver();
        // Attempt at localized explosion
        // Debug.Log(col.contacts[0].point.ToString()); 
        // transform.GetChild(0).position = col.contacts[0].point;
        transform.GetChild(0).GetComponent<Explosion>().SetGameOver(true);;
        _gameOver = true;
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        _gameMaster.AddScore();
    }
}
