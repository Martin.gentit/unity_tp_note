using System;
using UnityEngine;
using UnityEngine.UI;

public class DecorBehavior : MonoBehaviour
{
    private float _originalY;

    private void Start()
    {
        _originalY = transform.localPosition.y;
    }

    // Update is called once per frame
    void Update()
    {
        if (transform.localPosition.x >= 25.5f)
        {
            transform.localPosition = new Vector2(-25.6f, _originalY);
        }
        transform.Translate(Vector3.right/60);
    }
}
