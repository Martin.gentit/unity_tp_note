using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartGame : MonoBehaviour
{
    public TMP_Text startText;
    
    private Rigidbody2D _rb;
    private IEnumerator _animateStart;
    private IEnumerator _countdown;
    
    void Awake()
    {
        _rb = GetComponent<Rigidbody2D>();
        _animateStart = Countdown(3);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnMouseDown()
    {
        _rb.velocity += Vector2.right * 3;
        StartCoroutine(_animateStart);
    }

    IEnumerator Countdown(int seconds)
    {
        int count = seconds;
       
        while (count > 0) {
            startText.SetText(count.ToString());
            yield return new WaitForSeconds(1);
            count --;
        }
        
        SceneManager.LoadScene("Game", LoadSceneMode.Single);
    }
}
