using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using Random = System.Random;

public class GameMaster : MonoBehaviour
{
    public GameObject obstaclesPrefab;
    public TMP_Text score;

    private int _score;
    private Random _randomGenerator;
    private List<GameObject> _obstacles;
    private BoxCollider2D _player;
    
    // Start is called before the first frame update
    void Start()
    {
        _player = GameObject.Find("Player").GetComponent<BoxCollider2D>();
        _obstacles = new List<GameObject>();
        _randomGenerator = new Random();
        _obstacles.Add(Instantiate(obstaclesPrefab, new Vector3(-20, RandomY(), 0), Quaternion.identity));
        _obstacles.Add(Instantiate(obstaclesPrefab, new Vector3(-30, RandomY(), 0), Quaternion.identity));
        _obstacles.Add(Instantiate(obstaclesPrefab, new Vector3(-40, RandomY(), 0), Quaternion.identity));
    }

    // Update is called once per frame
    void Update()
    {
        HandleObstacleReset();
        score.SetText(_score.ToString());
    }

    int RandomY()
    {
        return _randomGenerator.Next(-2, 3);
    }

    void HandleObstacleReset()
    {
        foreach (GameObject obstacle in _obstacles)
        {
            if (obstacle.transform.position.x >= 10)
            {
                obstacle.transform.position = new Vector3(-20, RandomY(), 0);
            }
        }
    }

    public void AddScore()
    {
        _score += 1;
    }

    public void GameOver()
    {
        IEnumerator waitBeforeGameOver = WaitFor(1);
        StartCoroutine(waitBeforeGameOver);
    }
    
    IEnumerator WaitFor(int secs)
    {
        yield return new WaitForSeconds(secs);
        SceneManager.LoadScene("GameOver", LoadSceneMode.Single);
    }
}
